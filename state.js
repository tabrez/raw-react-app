var initNewContact = { name: '', email: '', description: '' };
var initState = {
  contacts: [
    { key: 1, name: 'James K Nelson', email: 'james@jamesknelson.com' },
    { key: 2, name: 'Jim', email: 'jim@example.com', description: 'ok' },
    { key: 3, name: 'Joe' }
  ],
  newContact: initNewContact,
  onNewContactChange: updateNewContact,
  onSubmitNewContact: submitNewContact,
  location: window.location.hash
};

var state = {};

var setState = function(newState) {
  Object.assign(state, newState);
  page = getRoute(state);

  ReactDOM.render(
    React.createElement(
      'div',
      {},
      React.createElement(page.component, page.compState),
      React.createElement('a', { href: '#/home' }, 'Home')
    ),
    document.getElementById('react-app')
  );
};
