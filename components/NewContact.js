// var createElement = function(ele, type, value, handler, placeholder, ref) {
//   return React.createElement(ele, {
//     type: type,
//     value: value,
//     placeholder: placeholder,
//     event: hander,
//     ref: ref
//   });
// };

var NewContact = React.createClass({
  propTypes: {
    contact: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    onSubmit: React.PropTypes.func.isRequired
  },

  componentDidUpdate: function(prevProps) {
    var contact = this.props.contact;
    var prevContact = prevProps.contact;
    if (
      this.isMounted &&
      contact.errors &&
      contact.errors != prevContact.errors
    ) {
      if (contact.errors.name) this.refs.name.focus();
      else if (contact.errors.email) this.refs.email.focus();
      return;
    }
  },

  onSubmit: function(e) {
    e.preventDefault();
    this.refs.name.focus();
    this.props.onSubmit(e);
  },

  onNameChange: function(event) {
    this.props.onChange(
      Object.assign({}, this.props.contact, { name: event.target.value })
    );
  },

  onEMailChange: function(event) {
    this.props.onChange(
      Object.assign({}, this.props.contact, { email: event.target.value })
    );
  },

  onDescriptionChange: function(event) {
    this.props.onChange(
      Object.assign({}, this.props.contact, { description: event.target.value })
    );
  },

  render: function() {
    var errors = this.props.contact.errors || {};
    return React.createElement(
      'h1',
      {},
      'Contact Form',
      React.createElement(
        'form',
        {
          className: 'ContactForm',
          onSubmit: this.onSubmit
        },
        React.createElement('input', {
          type: 'text',
          className: errors.name && 'ContactForm-error',
          placeholder: 'Name',
          value: this.props.contact.name,
          onChange: this.onNameChange,
          ref: 'name',
          autoFocus: true
        }),
        React.createElement(
          'div',
          { className: 'ContactForm-errormsg' },
          this.props.contact.errors ? this.props.contact.errors.name : ''
        ),
        React.createElement('input', {
          type: 'text',
          className: errors.email && 'ContactForm-error',
          placeholder: 'EMail',
          value: this.props.contact.email,
          onChange: this.onEMailChange,
          ref: 'email'
        }),
        React.createElement(
          'div',
          { className: 'ContactForm-errormsg' },
          this.props.contact.errors ? this.props.contact.errors.email : ''
        ),
        React.createElement('textarea', {
          placeholder: 'Description',
          value: this.props.contact.description,
          onChange: this.onDescriptionChange
        }),
        React.createElement(
          'button',
          {
            type: 'submit'
          },
          'Add Contact'
        )
      )
    );
  }
});
