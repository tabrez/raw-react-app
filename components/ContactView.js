var ContactView = React.createClass({
  propTypes: {
    contacts: React.PropTypes.array.isRequired,
    newContact: React.PropTypes.object.isRequired,
    onNewContactChange: React.PropTypes.func.isRequired,
    onSubmitNewContact: React.PropTypes.func.isRequired
  },

  render: function() {
    return React.createElement(
      'div',
      {},
      React.createElement(ContactList, {
        contacts: this.props.contacts
      }),
      React.createElement(NewContact, {
        contact: this.props.newContact,
        onChange: this.props.onNewContactChange,
        onSubmit: this.props.onSubmitNewContact
      })
    );
  }
});
