var ContactList = React.createClass({
  propTypes: {
    contacts: React.PropTypes.array.isRequired
  },

  listElements: function() {
    return this.props.contacts
      .filter(function(contact) {
        return contact.email;
      })
      .map(function(contact) {
        return React.createElement(ContactItem, contact);
      });
  },

  render: function() {
    return React.createElement(
      'div',
      { className: 'ContactView' },
      React.createElement(
        'h1',
        {
          className: 'ContactView-title'
        },
        'Contacts'
      ),
      React.createElement(
        'ul',
        { className: 'ContactView-list' },
        this.listElements()
      )
    );
  }
});
