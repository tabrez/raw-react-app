var PageNotFound = React.createClass({
  render: function() {
    return React.createElement(
      'div',
      {},
      React.createElement('p', {}, 'Page not found'),
      React.createElement('a', { href: '#/contacts' }, 'Contacts')
    );
  }
});
