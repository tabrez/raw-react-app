var Home = React.createClass({
  render: function() {
    return React.createElement(
      'ul',
      {},
      React.createElement(
        'li',
        {},
        React.createElement('a', { href: '#/contacts' }, 'Contacts')
      ),
      React.createElement(
        'li',
        {},
        React.createElement('a', { href: '#/new-contact' }, 'New Contact')
      ),
      React.createElement(
        'li',
        {},
        React.createElement('a', { href: '#/contact-list' }, 'Contact List')
      )
    );
  }
});
