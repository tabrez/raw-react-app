var getRoute = function(state) {
  var compState = state;
  var component = PageNotFound;
  switch (state.location) {
    case '#/contacts':
      component = ContactView;
      break;
    case '#/contact-list':
      compState = {
        contacts: state.contacts
      };
      component = ContactList;
      break;
    case '#/new-contact':
      compState = {
        contact: state.newContact,
        onChange: state.onNewContactChange,
        onSubmit: state.onSubmitNewContact
      };
      component = NewContact;
      break;
    case '#/home':
      component = Home;
      break;
  }
  return { component: component, compState: compState };
};

var navigated = function() {
  setState({ location: window.location.hash });
};
