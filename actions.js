var updateNewContact = function(contact) {
  setState({ newContact: contact });
};

var submitNewContact = function() {
  var newContact = Object.assign({}, state.newContact, {
    key: state.contacts.length + 1,
    errors: {}
  });

  if (!newContact.name)
    newContact.errors.name = 'Enter a name for the new contact';
  if (!newContact.email)
    newContact.errors.email = 'Enter an email for the new contact';

  if (Object.keys(newContact.errors).length === 0) {
    setState({
      contacts: state.contacts.slice(0).concat(newContact),
      newContact: initNewContact
    });
  } else setState({ newContact: newContact });
};
